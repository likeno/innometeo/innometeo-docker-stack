STACK_NAME=innometeo
DEPLOYMENT_ENVIRONMENT=${1:-dev}
GEOSPACA_REPO_ROOT=${GEOSPACA_REPO_ROOT:-$HOME/dev/geospaca}
SPATIAL_DATA_SERVER_REPO_ROOT=${SPATIAL_DATA_SERVER_REPO_ROOT:-$HOME/dev/innometeo/spatial-data-server}
MALMO_BACKEND_REPO_ROOT=${MALMO_BACKEND_REPO_ROOT:-$HOME/dev/innometeo/malmo}
MALMO_UI_REPO_ROOT=${MALMO_UI_REPO_ROOT:-$HOME/dev/innometeo/malmo-ui}
DATA_PROCESSING_REPO_ROOT=${DATA_PROCESSING_REPO_ROOT:-$HOME/dev/innometeo/data-processing-system}
FEATUREINFO_REPO_ROOT=${FEATUREINFO_REPO_ROOT:-$HOME/dev/innometeo/featureinfoserver}
DATA_PROCESSING_LOGS_DIR=${DATA_PROCESSING_LOGS_DIR:-$HOME/data/innometeo/data-processing-logs}
DATA_DIR=${DATA_DIR:-$HOME/data/innometeo}

echo "Current environment is $DEPLOYMENT_ENVIRONMENT"
echo "geospaca_repo_root=$GEOSPACA_REPO_ROOT"
echo "spatial_server_repo_root=$SPATIAL_DATA_SERVER_REPO_ROOT"

docker login registry.gitlab.com

if [ ${DEPLOYMENT_ENVIRONMENT} = production ]
then
  export DATA_PROCESSING_LOGS_DIR=$HOME/logs
  STACK_FILE_NAME=innometeo-stack-production.yml
  SPATIAL_DATA_SERVER_PUBLIC_URL=dataserver-innometeo.ipma.pt
else
  export SPATIAL_DATA_SERVER_REPO_ROOT=$SPATIAL_DATA_SERVER_REPO_ROOT
  export GEOSPACA_REPO_ROOT=$GEOSPACA_REPO_ROOT
  export MALMO_BACKEND_REPO_ROOT=$MALMO_BACKEND_REPO_ROOT
  export MALMO_UI_REPO_ROOT=$MALMO_UI_REPO_ROOT
  export DATA_PROCESSING_REPO_ROOT=$DATA_PROCESSING_REPO_ROOT
  export FEATUREINFO_REPO_ROOT=$FEATUREINFO_REPO_ROOT
  export DATA_PROCESSING_LOGS_DIR=$DATA_PROCESSING_LOGS_DIR
  export DATA_DIR=$DATA_DIR
  STACK_FILE_NAME=innometeo-stack-dev.yml
  SPATIAL_DATA_SERVER_PUBLIC_URL=dev-innometeo-spatial-data-server.lvh.me
fi

docker stack deploy \
    --with-registry-auth \
    --prune \
    --compose-file ${STACK_FILE_NAME} \
    ${STACK_NAME}

SLEEP_SECONDS=60
echo "Sleeping for ${SLEEP_SECONDS} seconds..."
sleep $SLEEP_SECONDS

echo "Use the following commands to initialize relevant resources:"
python3 initialize.py \
    --stack-name ${STACK_NAME} \
    --spatial-server-public-url ${SPATIAL_DATA_SERVER_PUBLIC_URL}
