import argparse
import shlex
import socket
import subprocess
import typing


def find_container_ids(
        stack_name: str,
        only_running: typing.Optional[bool] = False
) -> typing.Dict[str, typing.List[typing.Tuple[str, str]]]:
    completed_process = subprocess.run(
        shlex.split(f"docker stack ps {stack_name} --no-trunc"),
        capture_output=True,
        check=True,
        text=True,
    )
    container_ids = {}
    for line in completed_process.stdout.splitlines():
        fields = line.split()
        id_ = fields[0]
        name = fields[1]
        node = fields[3]
        current_state = fields[5].lower()
        service_name = name.partition("_")[-1].split(".")[0]
        container_name = ".".join((name, id_))
        if service_name != "":
            ids = container_ids.setdefault(service_name, [])
            details = (
                node,
                container_name,
            )
            if only_running:
                if current_state.startswith("running"):
                    ids.append(details)
            else:
                ids.append(details)
    return container_ids


def get_node_details(
        service_info: typing.List[typing.Tuple[str, str]]
) -> typing.Optional[typing.Tuple[str, str]]:
    local_node = socket.gethostname()
    for details in service_info:
        if details[0] == local_node:
            result = details
            break
    else:
        result = service_info[0]
    return result


def prepare_url_command(container: str, url: str):
    return (
        f"docker exec -ti {container} poetry run django-admin "
        f"adjust-site-domain {url!r}"
    )


def prepare_collections_loading_command(container: str) -> str:
    return (
        f"docker exec -ti {container} /bin/bash "
        f"/home/likeno/spatial-data-server/data-collections/"
        f"load-sample-data.sh"
    )


def prepare_malmo_service_registration_commands(
        container: str
) -> typing.List[str]:
    data_ = [
        ("ECMWF", "http://spatial-data-server-web:8000/ogc/nwp-ecmwf/"),
        ("AROME", "http://spatial-data-server-web:8000/ogc/nwp-arome/"),
        ("IPMA", "http://spatial-data-server-web:8000/ogc/ipma/"),
        ("LSASAF", "http://spatial-data-server-web:8000/ogc/lsasaf/"),
    ]
    result = []
    for layer_group, service_endpoint in data_:
        result.append(
            f"docker exec -ti {container} /bin/bash -c 'cd malmo && "
            f"python manage.py add-service-endpoint "
            f"--layer-group {layer_group} {service_endpoint}'"
        )
    return result


def prepare_airflow_backfill_command(container: str) -> str:
    return (
        f"docker exec -ti {container} airflow backfill "
        f"ecmwf_atlantic_pt "
        f"--start_date 2020-01-12T12:00:00 "
        f"--end_date 2020-01-13T12:00:00 "
        f"--task_regex publish "
        f"--ignore_dependencies "
        f"--reset_dagruns "
        f"--rerun_failed_tasks"
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--stack-name",
        default="innometeo"
    )
    parser.add_argument(
        "--spatial-server-public-url",
        default="https://dataserver-innometeo.ipma.pt"
    )
    args = parser.parse_args()
    stack_info = find_container_ids(args.stack_name)
    geospaca_web_details = get_node_details(
        stack_info["spatial-data-server-web"])
    public_url_command = prepare_url_command(
        geospaca_web_details[1], args.spatial_server_public_url)
    collections_command = prepare_collections_loading_command(
        geospaca_web_details[1])
    print(public_url_command)
    print(collections_command)
    malmo_details = get_node_details(stack_info["malmo-backend"])
    service_commands = prepare_malmo_service_registration_commands(
        malmo_details[1])
    for service_command in service_commands:
        print(service_command)
    airflow_worker_details = get_node_details(
        stack_info["data-processing-worker"])
    backfill_command = prepare_airflow_backfill_command(
        airflow_worker_details[1])
    print(backfill_command)
