# Innometeo docker stack

This repository holds docker-related configuration that is suitable for
deplying the various services used in the Innometeo project

## How to use this

This repository uses docker swarm. Be sure to have an existing swarm ready to
use.

1. Clone this repository locally on one of the swarm master nodes
1. Create `{repository-root}/secrets` directory and place the following files 
   inside it:
   
   ```sh
   DB_PASSWORD="db_pass"
   SECRET_KEY="something"
   ADMIN_PASSWORD="admin"
   
   printf "${DB_PASSWORD}" > secrets/spatial-data-server-database-password
   printf "postgresql://geospaca:${DB_PASSWORD}@spatial-data-server-database/geospaca" > secrets/spatial-data-server-web-database-url
   printf "amqp://guest:guest@message-queue:5672/" > secrets/spatial-data-server-web-message-queue-rabbitmq-url
   printf "${ADMIN_PASSWORD}" > secrets/malmo-backend-django-admin-password
   printf "postgresql://geospaca:${DB_PASSWORD}@malmo-database/geospaca" > secrets/malmo-backend-django-database-url
   printf "${SECRET_KEY}" > secrets/malmo-backend-django-secret-key
   ```
   
1. Login to the gitlab.com docker registry
1. Now deploy the stack. There are two differently named stack files, 
   applicable to dev or prod. Depending on your desired environment:

   -  If working in **development**, 
   
      -  The development stack file bind mounts the code of 
         `geospaca` and `innometeo/spatial-data-server`. In order to do this,
         it expects these to be located at `$HOME/dev/geospaca` and 
         `$HOME/dev/innometeo/spatial-data-server` respectively. If there are
         located elsewhere you must define the following environment variables:
         
         -  `GEOSPACA_REPO_ROOT`
         -  `SPATIAL_DATA_SERVER_REPO_ROOT`
   
      -  Then use the provided `deploy.sh` script
      
      ```
      bash deploy.sh
      ```
      
   -  For **production**, no additional code is bind mounted anywhere. Use the
      same `deploy.sh` script, with an appropriate flag
      
      ```
      bash deploy.sh production
      ```
      
      However, note that the production stack file expects the swarm to have
      nodes with the following labels:
      
      -  `pt.likeno.type=product-distribution` - services in the stack are 
          only deployed to nodes with this label
          
      -  `pt.likeno.database-handler=true` - there must be one node with this 
         label. This is where the postgres database(s) will be created and 
         persisted by means of a docker volume
         
      The production stack also expects to be able to create a volume with the
      [trajano/nfs-volume-plugin][] plugin. Therefore, this plugin must have 
      been previously installed.
      
[trajano/nfs-volume-plugin]: https://github.com/trajano/docker-volume-plugins/tree/master/nfs-volume-plugin
