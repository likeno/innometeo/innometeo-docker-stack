from string import Template

LAYERS = {
    'arome.wind_u.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_10u_merc_$year$month$day$run'),
            'name': 'u10',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }, {
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_u_merc_$year$month$day$run'),
            'name': 'u',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 950,
                'title': '950',
            }, {
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 700,
                'title': '700',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'arome.wind_v.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_10v_merc_$year$month$day$run'),
            'name': 'v10',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }, {
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_v_merc_$year$month$day$run'),
            'name': 'v',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 950,
                'title': '950',
            }, {
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 700,
                'title': '700',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'arome.temperature.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_2t_merc_$year$month$day$run'),
            'name': 't2m',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }, {
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_t_merc_$year$month$day$run'),
            'name': 't',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 950,
                'title': '950',
            }, {
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 700,
                'title': '700',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'arome.dewpoint.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_2d_merc_$year$month$day$run'),
            'name': 'd2m',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }]
    },

    'arome.high_cloud_cover.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_hcc_merc_$year$month$day$run'),
            'name': 'hcc',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }]
    },

    'arome.medium_cloud_cover.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_mcc_merc_$year$month$day$run'),
            'name': 'mcc',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }]
    },

    'arome.low_cloud_cover.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_lcc_merc_$year$month$day$run'),
            'name': 'lcc',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }]
    },

    'arome.total_cloud_cover.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_tcc_merc_$year$month$day$run'),
            'name': 'tcc',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }]
    },

    'arome.pressure.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_msl_merc_$year$month$day$run'),
            'name': 'msl',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }]
    },

    'arome.precipitation.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_tp_merc_$year$month$day$run'),
            'name': 'tp',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }]
    },

    'arome.vertical_velocity.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_w_merc_$year$month$day$run'),
            'name': 'w',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 950,
                'title': '950',
            }, {
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 700,
                'title': '700',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'arome.geopotential.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_z_merc_$year$month$day$run'),
            'name': 'z',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 950,
                'title': '950',
            }, {
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 700,
                'title': '700',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'arome.relative_humidity.continent': {
        'datasources': [{
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_r_2m-merc_$year$month$day$run'),
            'name': 'r',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }, {
            'path': Template('arome/$year/$month/$day/run_$run/AROME_OPER_001_FC_SP_PT2_025_r_merc_$year$month$day$run'),
            'name': 'r',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 950,
                'title': '950',
            }, {
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 700,
                'title': '700',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'ecmwf.wind_u.atlantic': {
        'datasources': [{
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_10u_merc_$year$month$day$run.nc'),
            'name': 'u10',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface'
            }]
        }, {
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_u_merc_$year$month$day$run.nc'),
            'name': 'u',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'ecmwf.wind_v.atlantic': {
        'datasources': [{
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_10v_merc_$year$month$day$run.nc'),
            'name': 'v10',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface'
            }]
        }, {
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_v_merc_$year$month$day$run.nc'),
            'name': 'v',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'ecmwf.temperature.atlantic': {
        'datasources': [{
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_2t_merc_$year$month$day$run.nc'),
            'name': 't2m',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface'
            }]
        }, {
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_t_merc_$year$month$day$run.nc'),
            'name': 't',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'ecmwf.relative_humidity.atlantic': {
        'datasources': [{
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_r_2m-merc_$year$month$day$run.nc'),
            'name': 'r',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface'
            }]
        }, {
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_r_merc_$year$month$day$run.nc'),
            'name': 'r',
            'levels': [{
                'name': 'isobaricInhPa',
                'value': 925,
                'title': '925',
            }, {
                'name': 'isobaricInhPa',
                'value': 900,
                'title': '900',
            }, {
                'name': 'isobaricInhPa',
                'value': 850,
                'title': '850',
            }, {
                'name': 'isobaricInhPa',
                'value': 800,
                'title': '800',
            }, {
                'name': 'isobaricInhPa',
                'value': 500,
                'title': '500',
            }, {
                'name': 'isobaricInhPa',
                'value': 300,
                'title': '300',
            }]
        }]
    },

    'ecmwf.pressure.atlantic': {
        'datasources': [{
            'path': Template('ecmwf/$year/$month/$day/run_$run/ECMWF_OPER_001_FC_SP_ATP_090_msl_merc_$year$month$day$run.nc'),
            'name': 'msl',
            'levels': [{
                'name': None,
                'value': None,
                'title': 'surface',
            }]
        }]
    },

}
